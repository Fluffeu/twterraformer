var farmSettings = {
    "maxWall": 1,
    "maxDist": 50,
    "notAttacked": true,
    "minUnitsSend": 2,
    "cFarmCountCap": 30,
    "reloadWaitScale": 2.0
}
var villagesData = []

var currentATemplate = {}
var currentBTemplate = {}
var nextTemplates = []
var autostarted = false
var attacksSent = 0

var unitsNotSent = 0 /* when autofarming, keep track of the number of units not sent, that will need to be send after template updates */

const TEMPLATE_UNITS = ["spear", "sword", "axe", "archer", "spy", "light", "marcher", "heavy", "knight"]


/* Add Autofarm button to bottom toolbar if we're on farm assistant's page */
if (/screen=am_farm/.test(location.href)) {
    addActivationButton()
}


function addActivationButton() {
    document.getElementById("linkContainer").innerHTML += `
     - 
    <a class="footer-link" id="twterraformer">Terraform</a>`

    document.getElementById("twterraformer").onclick = function() {
        runTerraformer()
    }
}


function runTerraformer() {
    // chrome.storage.local.set({sendImmediately: false})
    console.log("Running terraformer")
}


function postRun() {
    // console.log("farmPostRun")
    // // console.log("next templates: " + JSON.stringify(nextTemplate))
    // if (nextTemplates.length > 0 && villagesData.length > 0 && getUnitsSum(getAvailableUnits()) >= farmSettings["minUnitsSend"]) {
    //     waitForNextAction(farmSettings["reloadWaitScale"]).then( () => {
    //         chrome.storage.local.set({sendImmediately: true})
    //         changeTemplates()
    //     })
    // }
    // else if (autostarted || attacksSent > 0) {
    //     console.log("sending tooldone event")
    //     document.dispatchEvent(new Event("tool done"))
    // }
    // else {
    //     console.log("sending nothingtodo event")
    //     document.dispatchEvent(new Event("nothing to do"))
    // }
}
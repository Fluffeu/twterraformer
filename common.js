function clickButton(element) {
    element.dispatchEvent(new Event("focus"))
    // waitForNextAction(1, 10, 50).then(()=>{
    element.click()

    // })
}


function enterInputValue(element, value) {
    element.dispatchEvent(new Event("focus"))
    // waitForNextAction(1, 12, 15).then(()=>{
    element.dispatchEvent(new Event("keydown"))
    // waitForNextAction(1, 12, 15).then(()=>{
    element.value = value
    // waitForNextAction(1, 50, 70).then(()=>{
    element.dispatchEvent(new Event("keyup"))
    element.dispatchEvent(new Event("change"))

    // })})})
}


function waitForNextAction(delayScale = 1.0, min = 210, max = 800) {
    let rounds = 10
    let drift = 0.0
    for (let i = 0; i < rounds; i++) {
        drift += Math.random() 
    }
    drift = Math.abs(drift - rounds/2.0)
    drift /= rounds/2
    let delay = min + drift*(max - min)
    // console.log("delay: " + delay*delayScale)
    return new Promise(r => setTimeout(r, delay*delayScale))
}